import re
import sys
import os

with open('app/post_template', 'r') as t:
    pattern = re.compile(t.read())

new_posts = sys.argv[1:]

for new_post in new_posts:
    if os.path.exists(new_post):
        with open(new_post, 'r') as p:
            if not pattern.match(p.read()):
                print(f"{new_post} doesn't match!")
                sys.exit(1)

sys.exit(0)
