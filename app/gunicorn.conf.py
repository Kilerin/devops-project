#!/usr/bin/env python

from multiprocessing import cpu_count
from os import environ


def max_workers():
    return cpu_count() * 2 + 1


bind = "0.0.0.0:" + environ.get("FLASK_RUN_PORT", "8080")
workers = max_workers()
threads = 4
timeout = 30
graceful_timeout = 30
