"""empty message

Revision ID: 221ca768dded
Revises: 221ca768dded
Create Date: 2024-04-02 13:50:43.579296

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '221ca768dded'
down_revision = '221ca768dded'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
