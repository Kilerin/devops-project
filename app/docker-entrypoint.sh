#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres startup..."
    
    while ! nc -z $POSTGRES_HOST $POSTGRES_PORT; do
        sleep 0.1
    done

    echo "PostgreSQL successfully started"
fi

flask db migrate
flask db upgrade

exec "$@"
